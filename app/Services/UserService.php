<?php 

namespace App\Services;
use App\Contracts\UserServiceInterface;
use App\User;
use App\Models\Apple;

class UserService implements UserServiceInterface 
{

	/**
	 * Get all available users 
	 * @param  array $relations = []
	 * @return Collection
	*/
	public function getAllUsers($relations = [])
	{
		return User::with(User::getFiltratedRelaions($relations))->get();
	}

	/**
	 * make owner of apple 
	 * @param  int $apple_id = null
	 * @return void
	*/
	public function takeApple($user_id, $apple_id = null)
	{
		if(is_null($apple_id)) {
			$free_ids = Apple::whereNull('user_id')->select('id')->pluck('id');
			if(!$free_ids->isempty()) {
				$apple_id = $free_ids->random();
			}
		}
		if(!is_null($apple_id)) {
			Apple::where('id', $apple_id)->update(['user_id' => $user_id]);
		} 
	}

	/**
	 * Get single User
	 * @param int $id
	 * @param  array $relations = []
	 * @return Eloquent
	*/
	public function getUserById($id, $relations = [])
	{
		return User::where('id', $id)->with(User::getFiltratedRelaions($relations))->first();
	}

	/**
	 * detach apple from user
	 * @param  int $user_id
	 * @param  int $apple_id = null
	 * @return void
	*/
	public function resetApples($user_id, $apple_id = null)
	{
		$query = Apple::where('user_id', $user_id);
		if(!is_null($apple_id)) {
			$query->where('id', $apple_id);
		}
		$query->update(['user_id' => null]);
	}

}