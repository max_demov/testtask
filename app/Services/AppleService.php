<?php 

namespace App\Services;
use App\Contracts\AppleServiceInterface;
use App\Models\Apple;
use DateTime;

class AppleService implements AppleServiceInterface 
{
	/**
	 * Get all free apples 
	 * @return Collection
	*/
	public function getAvailableApples($paginate = true)
	{
		if($paginate) {
			return Apple::whereNull('user_id')->paginate(config('app.app_pagination_count'));
		}
		return Apple::whereNull('user_id')->get();
	}

	/**
	 * Add free apples 
	 * @param int $count = null
	 * @return void
	*/
	public function addApples($count = null)
	{
		$insertable = [];
		if(is_null($count)) {
			$count = 5;
		}
		for($i = 0; $i < $count; $i++) {
			$insertable[] = [
	            'created_at' => new DateTime(),
	            'updated_at' => new DateTime(),
	        ];
		}
		Apple::insert($insertable);
	}
}