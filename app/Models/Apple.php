<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apple extends Model
{
    protected $table = 'apples';

    public function user()
    {
    	return $this->hasOne('App\User');
    }
}
