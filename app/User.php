<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private static $available_relations = [
        'apples'
    ];

    public static function getFiltratedRelaions($relations)
    {
        $result = [];
        foreach ($relations as $value) {
            if(in_array($value, static::$available_relations)) {
                $result[] = $value;
            }
        }
        return $result;
    }

    /**
     * @param Apple $apple
     */

    public function apples()
    {
        return $this->hasMany('App\Models\Apple');
    }
}

