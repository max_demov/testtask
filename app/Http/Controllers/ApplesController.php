<?php

namespace App\Http\Controllers;

use App\Contracts\AppleServiceInterface;

class ApplesController extends Controller
{
	/**
	 * @param App\Contracts\AppleServiceInterface $appleService
	 * @return Response
	 */
    public function getFreeApples( AppleServiceInterface $appleService)
    {
     	$appleService->addApples();
     	return back();
    }
}
