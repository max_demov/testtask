<?php

namespace App\Http\Controllers;

use App\Contracts\UserServiceInterface;
use App\Contracts\AppleServiceInterface;

class HomeController extends Controller
{
    /**
     * @param App\Contracts\AppleServiceInterface $appleService
     * @param App\Contracts\UserServiceInterface $userService
     * @return Response
     */
    public function index(AppleServiceInterface $appleService, UserServiceInterface $userService)
    {
        $users = $userService->getAllUsers(['apples']);
        $basket_apples = $appleService->getAvailableApples();
        return view('site.home', [
            'users' => $users,
            'basket_apples' => $basket_apples
        ]); 
    }
}
