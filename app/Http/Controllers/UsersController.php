<?php

namespace App\Http\Controllers;

use App\Contracts\UserServiceInterface;

class UsersController extends Controller
{

    /**
     * @param int $id
     * @param App\Contracts\UserServiceInterface $userService
     * @return Response
     */
    public function show($id, UserServiceInterface $userService)
    {
     	$user = $userService->getUserById($id);
     	$users = $userService->getAllUsers(['apples']);
        $apples = $user->apples()->paginate(config('app.app_pagination_count'));
     	return view('site.users.index', [
     		'single_user' => $user,
     		'users' => $users,
            'apples' => $apples
     	]); 
    }

    /**
     * @param int $user_id
     * @param App\Contracts\UserServiceInterface $userService
     * @return Response
     */
    public function takeApple( $user_id, UserServiceInterface $userService ) 
    {
        $userService->takeApple($user_id);
        return redirect('/');
    }

    /**
     * @param int $user_id
     * @param App\Contracts\UserServiceInterface $userService
     * @return Response
     */
    public function resetApples( $user_id, UserServiceInterface $userService ) 
    {
        $userService->resetApples($user_id);
        return redirect('/');
    }

    /**
     * @param int $user_id
     * @param int $apple_id
     * @param App\Contracts\UserServiceInterface $userService
     * @return Response
     */
    public function resetSingleApple( $user_id, $apple_id, UserServiceInterface $userService ) 
    {
        $userService->resetApples($user_id, $apple_id);
        return back();
    }
}
