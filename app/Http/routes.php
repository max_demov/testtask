<?php

Route::get('/', 'HomeController@index' );
Route::get('/take-apple/{user_id}', 'UsersController@takeApple' );
Route::get('/reset-apples/{user_id}', 'UsersController@resetApples' );
Route::get('/users/{user_id}/reset-apple/{apple_id}', 'UsersController@resetSingleApple' );
Route::get('/free-apples', 'ApplesController@getFreeApples' );
Route::resource('users', 'UsersController', ['only' => ['show']] );

