<?php 

namespace App\Contracts;

interface UserServiceInterface 
{
	/**
	 * Get all available users 
	 * @param  array $relations = []
	 * @return Collection
	*/
	public function getAllUsers($relations = []);

	/**
	 * make owner of apple 
	 * @param  int $user_id
	 * @param  int $apple_id = null
	 * @return void
	*/
	public function takeApple($user_id, $apple_id = null);

	/**
	 * detach apple(s) from user
	 * @param  int $user_id
	 * @param  int $apple_id = null
	 * @return void
	*/
	public function resetApples($user_id, $apple_id = null);

	/**
	 * Get single user
	 * @param int $id
	 * @param  array $relations = []
	 * @return Illuminate\Database\Eloquent\Model
	*/
	public function getUserById($id, $relations = []);

}