<?php 

namespace App\Contracts;

interface AppleServiceInterface 
{
	/**
	 * Get all free apples 
	 * @param boolean $paginate = true
	 * @return Collection
	*/
	public function getAvailableApples($paginate = true);

	/**
	 * Add free apples 
	 * @param int $count = null
	 * @return void
	*/
	public function addApples($count = null);
}