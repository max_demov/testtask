@extends('app')

@section('content')


    <div class="navbar navbar-default ">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><b>SITE</b></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                </ul>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="col-md-6">
            <h3>Users</h3>
            <ul class="list-group">
                @foreach( $users as $user )
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-sm-8">
                                <a href="{{url('/users/'.$user->id)}}">{{ $user->name }}</a> has {{ $user->apples->count() }} apples         
                            </div>
                            <div class="col-sm-4">
                                <div class="pull-right">
                                    <a class="btn btn-xs btn-danger" onclick="return confirm('Are you sure ?')" href="{{ url('/reset-apples/'.$user->id) }}">Reset</a>
                                    <a class="btn btn-xs btn-success" href="{{ url('/take-apple/'.$user->id) }}">Grab apple</a>
                                </div>   
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>


        <div class="col-md-6">

            <h3>Basket ({{$basket_apples->total()}})</h3>
            <ul class="list-group">
                @if(count($basket_apples) > 0)
                    @foreach( $basket_apples as $key => $apple )
                        <li class="list-group-item">
                            Apple {{$apple->id}}
                        </li>
                    @endforeach
                @else 
                    @include('alerts.no-result')
                    <div class="centered">
                        <a class="btn btn-lg btn-success text-center" href="{{ url('/free-apples') }}">Add Apples</a>
                    </div>
                @endif
            </ul>
            @if(count($basket_apples) > 0)
                {!! $basket_apples->render() !!}
            @endif
        </div>

    </div>


@endsection


