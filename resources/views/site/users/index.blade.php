@extends('app')

@section('content')


    <div class="navbar navbar-default ">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><b>SITE</b></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                </ul>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="col-md-6">
            <h3>Users</h3>
            <ul class="list-group">
                @foreach( $users as $user )
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-sm-8">
                                <a href="{{url('/users/'.$user->id)}}">{{ $user->name }}</a> has {{ $user->apples->count() }} apples         
                            </div>
                            <div class="col-sm-4">
                                <div class="pull-right">
                                    <a class="btn btn-xs btn-danger" onclick="return confirm('Are you sure ?')" href="{{ url('/reset-apple/'.$user->id) }}">Reset</a>
                                    <a class="btn btn-xs btn-success" href="{{ url('/take-apple/'.$user->id) }}">Grab apple</a>
                                </div>   
                            </div>
                        </div>
                    </li>
                @endforeach

            </ul>
            <div class="centered">
                <a class="btn btn-lg btn-success text-center" href="{{ url('/') }}">Free Apples</a>
            </div>
        </div>


        <div class="col-md-6">

            <h3>Basket {{$single_user->name}} ({{$apples->total()}})</h3>
            <ul class="list-group">
                @if(count($apples) > 0)
                    @foreach( $apples as $key => $apple )
                        <li class="list-group-item">
                            Apple {{$apple->id}}
                            <a class="btn btn-xs btn-danger pull-right" onclick="return confirm('Are you sure ?')" href="{{ url('/users/'.$single_user->id.'/reset-apple/'.$apple->id) }}">Remove</a>
                        </li>
                    @endforeach
                @else 
                    @include('alerts.no-result')
                @endif
            </ul>
            @if(count($apples) > 0)
                {!! $apples->render() !!}
            @endif
        </div>

    </div>


@endsection


