<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserServiceInterfaceTest extends TestCase
{
   
    public function testGetAllUsers()
    {
        \Artisan::call('migrate');
        \Artisan::call('db:seed');
        $relation_name = 'apples';
        $users_count = \DB::table('users')->count();
        $userService = app('App\Contracts\UserServiceInterface');
        $users = $userService->getAllUsers();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $users);
        $this->assertEquals($users_count, count($users));
        if(count($users) > 0) {
	        $this->assertEquals(false, $users[0]->relationLoaded($relation_name));
	        $this->assertEquals(true, $userService->getAllUsers([$relation_name])[0]->relationLoaded($relation_name));
        }
    }

    public function testGetUserById()
    {
        \Artisan::call('migrate');
        \Artisan::call('db:seed');
        $relation_name = 'apples';
        $user = \DB::table('users')->first();
        $user_id = $user->id;
        $userService = app('App\Contracts\UserServiceInterface');
        $user = $userService->getUserById($user_id);
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Model', $user);
        $this->assertEquals($user_id, $user->id);
        $this->assertEquals(false, $user->relationLoaded($relation_name));
        $this->assertEquals(true, $userService->getUserById($user_id, [$relation_name])->relationLoaded($relation_name));
    }

    public function testTakeApple()
    {
    	\Artisan::call('migrate');
    	\Artisan::call('db:seed');
    	$user = \DB::table('users')->first();
    	$user_id = $user->id;
    	$userService = app('App\Contracts\UserServiceInterface');
    	$user = $userService->getUserById($user_id);
    	$this->assertEquals(0, count($user->apples));
    	$apple = \DB::table('apples')->first();
    	$userService->takeApple($user_id, $apple->id);
    	$user->load('apples');
    	$this->assertEquals(1, count($user->apples));
    	$this->assertEquals($apple->id, $user->apples[0]->id);
    }

    public function testResetApples()
    {
    	\Artisan::call('migrate');
    	\Artisan::call('db:seed');
    	$user = \DB::table('users')->first();
    	$user_id = $user->id;
    	$userService = app('App\Contracts\UserServiceInterface');
    	$user = $userService->getUserById($user_id);
    	$apple = \DB::table('apples')->first();
    	$userService->takeApple($user_id, $apple->id);
    	$user->load('apples');
    	$this->assertEquals(1, count($user->apples));
    	$userService->resetApples($user->id, $apple->id);
    	$user->load('apples');
    	$this->assertEquals(0, count($user->apples));
    }
}
