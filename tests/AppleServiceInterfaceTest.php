<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AppleServiceInterfaceTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testGetAvailableApples()
    {
        \Artisan::call('migrate');
        \Artisan::call('db:seed');
        $initial_count = \DB::table('apples')->count();
        $appleService = app('App\Contracts\AppleServiceInterface');
        $available_apples = $appleService->getAvailableApples(false);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $available_apples);
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $appleService->getAvailableApples(true));
        $this->assertEquals($initial_count, count($available_apples));
        $user = \DB::table('users')->first();
        DB::table('apples')->update(['user_id' => $user->id]);        
        $this->assertNotEquals($initial_count, count($appleService->getAvailableApples(false)));
    }

    public function testAddApples()
    {
        \Artisan::call('migrate');
        \Artisan::call('db:seed');
        $initial_count = \DB::table('apples')->count();
        $appleService = app('App\Contracts\AppleServiceInterface');
        $available_apples = $appleService->getAvailableApples(false);
        $initial_count = count($available_apples);
        $appleService->addApples(1);
        $available_apples = $appleService->getAvailableApples(false);
        $this->assertEquals($initial_count + 1, count($available_apples));
    }
}
